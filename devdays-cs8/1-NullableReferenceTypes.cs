﻿using NUnit.Framework;

namespace devdays_cs8
{
    class NullableReferenceTypes
    {
        [Test]
        public void ValueTypes()
        {
            // not allowed
            //int nonNullable = null;
            //Assert.IsNull(nonNullable);

            int? nullable = null;
            Assert.IsNull(nullable);
        }

//#nullable enable
        [Test]
        public void ReferenceTypes()
        {
            string nonNullable = null;
            string? nullable = null;

            nonNullable = nullable;

            Assert.IsNull(nonNullable);
            Assert.IsNull(nullable);
            Assert.IsNotNull(GetLength(nullable));
        }

        private int GetLength(string? nullable)
        {
            return nullable?.Length ?? 0;
        }
//#nullable restore
    }
}
