﻿using NUnit.Framework;
using System;
using System.Runtime.CompilerServices;

namespace devdays_cs8
{
    class PatternMatching
    {
        interface IEnemy
        {
            int Health { get; set; }
        }

        class Enemy : IEnemy
        {
            public int Health { get; set; }
        }

        interface IWeapon
        {
            int Damage { get; set; }
            void Attack(IEnemy enemy);
            void Repair();
        }

        class Sword : IWeapon
        {
            public int Damage { get; set; }
            public int Durability { get; set; }

            public void Attack(IEnemy enemy)
            {
                if (Durability > 0)
                {
                    enemy.Health -= Damage;
                    Durability--;
                }
            }

            public void Repair()
            {
                Durability += 100;
            }

            public void Deconstruct(out int damage, out int durability)
            {
                damage = Damage;
                durability = Durability;
            }
        }

        class Bow : IWeapon
        {
            public int Damage { get; set; }
            public int Arrows { get; set; }

            public void Attack(IEnemy enemy)
            {
                if (Arrows > 0)
                {
                    enemy.Health -= Damage;
                    Arrows--;
                }
            }

            public void Repair()
            { }
        }

        enum State
        {
            Running,
            Suspended,
            NotRunning
        }

        enum Transition
        {
            Suspend,
            Resume,
            Terminate,
            Activate
        }

        [Test]
        public void TypePattern()
        {
            IWeapon weapon = new Sword { Damage = 5, Durability = 1 };
            IEnemy enemy = new Enemy { Health = 10 };

            switch (weapon)
            {
                case Sword sword when sword.Durability > 0:
                    enemy.Health -= sword.Damage;
                    sword.Durability--;
                    break;
                case Bow bow when bow.Arrows > 0:
                    enemy.Health -= bow.Damage;
                    bow.Arrows--;
                    break;
            }

            Assert.AreEqual(5, enemy.Health);
        }

        [Test]
        public void TuplePattern()
        {
            State state = State.Running;
            Transition transition = Transition.Suspend;

            switch (state, transition)
            {
                case (State.Running, Transition.Suspend):
                    state = State.Suspended;
                    break;
                case (State.Suspended, Transition.Resume):
                    state = State.Running;
                    break;
                case (State.Suspended, Transition.Terminate):
                    state = State.NotRunning;
                    break;
                case (State.NotRunning, Transition.Activate):
                    state = State.Running;
                    break;
                default:
                    throw new InvalidOperationException();
            }

            Assert.AreEqual(State.Suspended, state);
        }

        [Test]
        public void SwitchExpression()
        {
            State state = State.Running;
            Transition transition = Transition.Suspend;

            state = (state, transition) switch
            {
                (State.Running, Transition.Suspend) => State.Suspended,
                (State.Suspended, Transition.Resume) => State.Running,
                (State.Suspended, Transition.Terminate) => State.NotRunning,
                (State.NotRunning, Transition.Activate) => State.Running,
                _ => throw new InvalidOperationException(),
            };
            Assert.AreEqual(State.Suspended, state);
        }

        [Test]
        public void SwitchExpressionMissingCase()
        {
            State state = State.Running;
            Transition transition = Transition.Activate;

            Assert.Throws<SwitchExpressionException>(() =>
            {
                state = (state, transition) switch
                {
                    (State.Running, Transition.Suspend) => State.Suspended,
                    (State.Suspended, Transition.Resume) => State.Running,
                    (State.Suspended, Transition.Terminate) => State.NotRunning,
                    (State.NotRunning, Transition.Activate) => State.Running,
                };
            });
        }

        [Test]
        public void PropertyPattern()
        {
            var sword = new Sword { Damage = 10 };

            if (sword is Sword { Damage: 10, Durability: var durability })
            {
                Assert.AreEqual(sword.Durability, durability);
            }
            else
            {
                Assert.Fail();
            }
        }

        [Test]
        public void PositionalPattern()
        {
            var sword = new Sword { Damage = 10 };

            if (sword is Sword(10, var durability))
            {
                Assert.AreEqual(sword.Durability, durability);
            }
            else
            {
                Assert.Fail();
            }
        }
    }
}
