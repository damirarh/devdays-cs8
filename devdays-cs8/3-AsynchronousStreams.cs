﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace devdays_cs8
{
    class AsynchronousStreams
    {
        private readonly string inputFile = Path.Combine(TestContext.CurrentContext.TestDirectory, "lines.txt");

        private IEnumerable<string> ReadLines(string path)
        {
            using (var reader = new StreamReader(path))
            {
                var line = reader.ReadLine();
                while (line != null)
                {
                    yield return line;
                    line = reader.ReadLine();
                }
            }
        }

        [Test]
        public void SynchronousIterator()
        {
            foreach (var line in ReadLines(inputFile))
            {
                Console.WriteLine(line);
            }
            Assert.Pass();
        }

        private async Task<IEnumerable<string>> ReadLinesAllAsync(string path)
        {
            var lines = new List<string>();
            using (var reader = new StreamReader(path))
            {
                var line = await reader.ReadLineAsync();
                while (line != null)
                {
                    lines.Add(line);
                    line = await reader.ReadLineAsync();
                }
            }
            return lines;
        }

        [Test]
        public async Task Asynchronous()
        {
            foreach (var line in await ReadLinesAllAsync(inputFile))
            {
                Console.WriteLine(line);
            }
            Assert.Pass();
        }

        private async IAsyncEnumerable<string> ReadLinesAsync(string path)
        {
            using (var reader = new StreamReader(path))
            {
                var line = await reader.ReadLineAsync();
                while (line != null)
                {
                    yield return line;
                    line = await reader.ReadLineAsync();
                }
            }
        }

        [Test]
        public async Task AsynchronousStream()
        {
            await foreach (var line in ReadLinesAsync(inputFile))
            {
                Console.WriteLine(line);
            }
            Assert.Pass();
        }
    }
}
