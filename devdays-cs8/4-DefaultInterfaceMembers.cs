﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Xml.Linq;

namespace devdays_cs8
{
    class DefaultInterfaceMembers
    {
        enum LogLevel
        {
            Debug,
            Information,
            Error
        }

        interface ILogger
        {
            void Log(LogLevel level, string message);

            void Log(Exception exception) => Log(LogLevel.Error, exception.Message);
        }

        class Logger : ILogger
        {
            private readonly StringBuilder log = new StringBuilder();

            public string Contents => log.ToString();

            public void Log(LogLevel level, string message)
            {
                log.AppendLine($"{level}: {message}");
            }
        }

        [Test]
        public void MessageLogging()
        {
            var message = "Logging works";
            var logger = new Logger();

            logger.Log(LogLevel.Information, message);

            Assert.AreEqual($"Information: {message}" + Environment.NewLine, logger.Contents);
        }

        [Test]
        public void ExceptionLogging()
        {
            var exception = new Exception("Error occurred");
            ILogger logger = new Logger();

            logger.Log(exception);

            Assert.AreEqual($"Error: {exception.Message}" + Environment.NewLine, ((Logger)logger).Contents);
        }

        interface IXmlSerializable
        {
            IDictionary<string, string> Properties { get; }
            string Serialize()
            {
                return new XDocument(
                    new XElement("properties",
                        Properties.Select(pair => new XElement(pair.Key, pair.Value))
                    )
                ).ToString(SaveOptions.DisableFormatting);
            }
        }

        interface IJsonSerializable
        {
            IDictionary<string, string> Properties { get; }
            string Serialize()
            {
                using (var stream = new MemoryStream())
                {
                    using (var jsonWriter = new Utf8JsonWriter(stream))
                    {
                        jsonWriter.WriteStartObject();
                        foreach (var pair in Properties)
                        {
                            jsonWriter.WriteString(pair.Key, pair.Value);
                        }
                        jsonWriter.WriteEndObject();
                    }
                    stream.Position = 0;
                    using var reader = new StreamReader(stream);
                    return reader.ReadToEnd();
                }
            }
        }

        class Bag : IXmlSerializable, IJsonSerializable
        {
            public Bag(IDictionary<string, string> properties)
            {
                Properties = properties;
            }

            public IDictionary<string, string> Properties { get; set; }
        }

        [Test]
        public void XmlSerialize()
        {
            IXmlSerializable bag = new Bag(new Dictionary<string, string>(){
                { "key1", "value1" },
                { "key2", "value2" }
            });

            var expected = "<properties><key1>value1</key1><key2>value2</key2></properties>";
            Assert.AreEqual(expected, bag.Serialize());
        }

        [Test]
        public void JsonSerialize()
        {
            IJsonSerializable bag = new Bag(new Dictionary<string, string>(){
                { "key1", "value1" },
                { "key2", "value2" }
            });

            var expected = @"{""key1"":""value1"",""key2"":""value2""}";
            Assert.AreEqual(expected, bag.Serialize());
        }
    }
}
